# Copyright (C) 2001-2018, Python Software Foundation
# For licence information, see README file.
#
msgid ""
msgstr ""
"Project-Id-Version: Python 3\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-09-23 16:16+0200\n"
"PO-Revision-Date: 2022-11-17 10:01+0100\n"
"Last-Translator: Jeffd <dev@zest-labs.fr>\n"
"Language-Team: FRENCH <traductions@lists.afpy.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: library/graphlib.rst:2
msgid ":mod:`graphlib` --- Functionality to operate with graph-like structures"
msgstr ""
":mod:`graphlib` — Fonctionnalités pour travailler avec des structures de "
"type graphe"

#: library/graphlib.rst:8
msgid "**Source code:** :source:`Lib/graphlib.py`"
msgstr "**Code source:** :source:`Lib/graphlib.py`"

#: library/graphlib.rst:20
msgid "Provides functionality to topologically sort a graph of hashable nodes."
msgstr ""
"Fournit les fonctionnalités pour trier topologiquement un graphe de nœuds "
"hachables."

#: library/graphlib.rst:22
msgid ""
"A topological order is a linear ordering of the vertices in a graph such "
"that for every directed edge u -> v from vertex u to vertex v, vertex u "
"comes before vertex v in the ordering. For instance, the vertices of the "
"graph may represent tasks to be performed, and the edges may represent "
"constraints that one task must be performed before another; in this example, "
"a topological ordering is just a valid sequence for the tasks. A complete "
"topological ordering is possible if and only if the graph has no directed "
"cycles, that is, if it is a directed acyclic graph."
msgstr ""
"L'ordre topologique est un ordre linéaire des sommets d'un graphe afin que "
"pour chaque arête u → v d'un sommet u à un sommet v, cet ordre va placer le "
"sommet u avant le sommet v. Par exemple, les sommets d'un graphe peuvent "
"représenter une tâche à faire et une arête peut représenter la contrainte "
"comme quoi telle tâche doit être réalisée avant telle autre. Dans cet "
"exemple, un ordre topologique est simplement une séquence valide pour ces "
"tâches. Cet ordre n'est possible que si le graphe n'a pas de circuit, c'est-"
"à-dire si c'est un graphe orienté acyclique."

#: library/graphlib.rst:31
msgid ""
"If the optional *graph* argument is provided it must be a dictionary "
"representing a directed acyclic graph where the keys are nodes and the "
"values are iterables of all predecessors of that node in the graph (the "
"nodes that have edges that point to the value in the key). Additional nodes "
"can be added to the graph using the :meth:`~TopologicalSorter.add` method."
msgstr ""
"Si l'argument optionnel *graph* est fourni, cela doit être un dictionnaire "
"représentant un graphe acyclique avec comme clés les nœuds et comme valeurs "
"des itérables sur les prédécesseurs de ces nœuds dans le graphe (les nœuds "
"qui ont des arêtes qui pointent vers la valeur de la clé). Les nœuds "
"s'ajoutent en utilisant la méthode :meth:`~TopologicalSorter.add`"

#: library/graphlib.rst:37
msgid ""
"In the general case, the steps required to perform the sorting of a given "
"graph are as follows:"
msgstr ""
"De manière générale, les étapes nécessaires pour trier un graphe donné sont "
"les suivantes :"

#: library/graphlib.rst:40
msgid ""
"Create an instance of the :class:`TopologicalSorter` with an optional "
"initial graph."
msgstr ""
"créer une instance de la classe :class:`TopologicalSorter` avec "
"éventuellement un graphe initial ;"

#: library/graphlib.rst:42
msgid "Add additional nodes to the graph."
msgstr "ajouter d'autres nœuds au graphe ;"

#: library/graphlib.rst:43
msgid "Call :meth:`~TopologicalSorter.prepare` on the graph."
msgstr "appeler :meth:`~TopologicalSorter.prepare` sur le graphe ;"

#: library/graphlib.rst:44
msgid ""
"While :meth:`~TopologicalSorter.is_active` is ``True``, iterate over the "
"nodes returned by :meth:`~TopologicalSorter.get_ready` and process them. "
"Call :meth:`~TopologicalSorter.done` on each node as it finishes processing."
msgstr ""
"tant que :meth:`~TopologicalSorter.is_active` est à ``True``, itérer sur les "
"nœuds renvoyés par :meth:`~TopologicalSorter.get_ready` pour les traiter. "
"Appeler :meth:`~TopologicalSorter.done` sur chaque nœud une fois le "
"traitement terminé."

#: library/graphlib.rst:49
msgid ""
"In case just an immediate sorting of the nodes in the graph is required and "
"no parallelism is involved, the convenience method :meth:`TopologicalSorter."
"static_order` can be used directly:"
msgstr ""
"Si vous souhaitez simplement trier des nœuds du graphe sans parallélisme, la "
"méthode :meth:`TopologicalSorter.static_order` peut être utilisée "
"directement :"

#: library/graphlib.rst:60
msgid ""
"The class is designed to easily support parallel processing of the nodes as "
"they become ready. For instance::"
msgstr ""
"La classe est conçue pour prendre facilement en charge le traitement en "
"parallèle des nœuds quand ils deviennent disponibles. Par exemple ::"

#: library/graphlib.rst:87
msgid ""
"Add a new node and its predecessors to the graph. Both the *node* and all "
"elements in *predecessors* must be hashable."
msgstr ""
"Ajoute un nouveau nœud et son prédécesseur dans le graphe. Le *node* ainsi "
"que tous les éléments dans *predecessors* doivent être hachables."

#: library/graphlib.rst:90
msgid ""
"If called multiple times with the same node argument, the set of "
"dependencies will be the union of all dependencies passed in."
msgstr ""
"S'il est appelé plusieurs fois avec le même nœud en tant qu'argument, "
"l'ensemble des dépendances sera l'union de toutes les dépendances qui auront "
"été transmises."

#: library/graphlib.rst:93
msgid ""
"It is possible to add a node with no dependencies (*predecessors* is not "
"provided) or to provide a dependency twice. If a node that has not been "
"provided before is included among *predecessors* it will be automatically "
"added to the graph with no predecessors of its own."
msgstr ""
"Il est possible d'ajouter un nœud sans dépendance (*predecessors* n'est pas "
"fourni) ou de fournir une dépendance deux fois. Si un nœud qui n'a jamais "
"été fourni auparavant est inclus dans *predecessors* il sera automatiquement "
"ajouté au graphe sans prédécesseur lui-même."

#: library/graphlib.rst:98
msgid ""
"Raises :exc:`ValueError` if called after :meth:`~TopologicalSorter.prepare`."
msgstr ""
"Lève une :exc:`ValueError` si appelée après :meth:`~TopologicalSorter."
"prepare`."

#: library/graphlib.rst:102
msgid ""
"Mark the graph as finished and check for cycles in the graph. If any cycle "
"is detected, :exc:`CycleError` will be raised, but :meth:`~TopologicalSorter."
"get_ready` can still be used to obtain as many nodes as possible until "
"cycles block more progress. After a call to this function, the graph cannot "
"be modified, and therefore no more nodes can be added using :meth:"
"`~TopologicalSorter.add`."
msgstr ""
"Indique que le graphe est terminé et vérifie les circuits du graphe. Si un "
"circuit est détecté, une :exc:`CycleError` est levée mais :meth:"
"`~TopologicalSorter.get_ready` peut encore être utilisée pour obtenir autant "
"de nœuds que possible avant que les circuits ne bloquent la progression. "
"Après un appel de cette fonction, le graphe ne peut pas être modifié, et "
"donc aucun nœud ne peut être ajouté avec :meth:`~TopologicalSorter.add`."

#: library/graphlib.rst:111
msgid ""
"Returns ``True`` if more progress can be made and ``False`` otherwise. "
"Progress can be made if cycles do not block the resolution and either there "
"are still nodes ready that haven't yet been returned by :meth:"
"`TopologicalSorter.get_ready` or the number of nodes marked :meth:"
"`TopologicalSorter.done` is less than the number that have been returned by :"
"meth:`TopologicalSorter.get_ready`."
msgstr ""
"Renvoie ``True`` si une progression peut être faite et ``False`` dans le cas "
"contraire. La progression est possible si des circuits ne bloquent pas la "
"résolution ou qu'il reste des nœuds prêts qui n'ont pas encore été renvoyés "
"par :meth:`TopologicalSorter.get_ready` ou que le nombre de nœuds marqués :"
"meth:`TopologicalSorter.done` est inférieur au nombre qui a été renvoyé par :"
"meth:`TopologicalSorter.get_ready`."

#: library/graphlib.rst:118
msgid ""
"The :meth:`~TopologicalSorter.__bool__` method of this class defers to this "
"function, so instead of::"
msgstr ""
"La méthode :meth:`~TopologicalSorter.__bool__` de cette classe renvoie à "
"cette fonction donc au lieu de ::"

#: library/graphlib.rst:124
msgid "it is possible to simply do::"
msgstr "il est plus simple de faire ::"

#: library/graphlib.rst:152
msgid ""
"Raises :exc:`ValueError` if called without calling :meth:`~TopologicalSorter."
"prepare` previously."
msgstr ""
"Lève une :exc:`ValueError` si l'appel à :meth:`~TopologicalSorter.prepare` "
"n'a pas été fait au préalable."

#: library/graphlib.rst:134
msgid ""
"Marks a set of nodes returned by :meth:`TopologicalSorter.get_ready` as "
"processed, unblocking any successor of each node in *nodes* for being "
"returned in the future by a call to :meth:`TopologicalSorter.get_ready`."
msgstr ""
"Marque un ensemble de nœuds renvoyé par :meth:`TopologicalSorter.get_ready` "
"comme traités, permettant aux successeurs de chaque nœud de *nodes* d'être "
"renvoyés lors d'un prochain appel à :meth:`~TopologicalSorter.get_ready`."

#: library/graphlib.rst:138
msgid ""
"Raises :exc:`ValueError` if any node in *nodes* has already been marked as "
"processed by a previous call to this method or if a node was not added to "
"the graph by using :meth:`TopologicalSorter.add`, if called without calling :"
"meth:`~TopologicalSorter.prepare` or if node has not yet been returned by :"
"meth:`~TopologicalSorter.get_ready`."
msgstr ""
"Lève une :exc:`ValueError` si n'importe quel nœud dans *nodes* a déjà été "
"marqué comme traité par un précédent appel à cette méthode ou si un nœud n'a "
"pas été ajouté au graphe en utilisant :meth:`TopologicalSorter.add`, si "
"l'appel est fait sans appel à :meth:`~TopologicalSorter.prepare` ou si le "
"nœud n'a pas encore été renvoyé par :meth:`~TopologicalSorter.get_ready`."

#: library/graphlib.rst:146
msgid ""
"Returns a ``tuple`` with all the nodes that are ready. Initially it returns "
"all nodes with no predecessors, and once those are marked as processed by "
"calling :meth:`TopologicalSorter.done`, further calls will return all new "
"nodes that have all their predecessors already processed. Once no more "
"progress can be made, empty tuples are returned."
msgstr ""
"Renvoie un *n*-uplet avec tous les nœuds prêts. Renvoie d'abord tous les "
"nœuds sans prédécesseurs, et une fois marqués comme traités avec un appel "
"de :meth:`TopologicalSorter.done`, les autres appels renvoient tous les "
"nouveaux nœuds dont tous les prédécesseurs sont traités. Une fois que la "
"progression n'est plus possible, des tuples vides sont renvoyés."

#: library/graphlib.rst:157
#, fuzzy
msgid ""
"Returns an iterator object which will iterate over nodes in a topological "
"order. When using this method, :meth:`~TopologicalSorter.prepare` and :meth:"
"`~TopologicalSorter.done` should not be called. This method is equivalent "
"to::"
msgstr ""
"Renvoie un itérable contenant les nœuds dans un ordre topologique. "
"L'utilisation de cette méthode permet d'éviter l'appel à :meth:"
"`TopologicalSorter.prepare` ou :meth:`TopologicalSorter.done` Cette méthode "
"est équivalente à ::"

#: library/graphlib.rst:169
msgid ""
"The particular order that is returned may depend on the specific order in "
"which the items were inserted in the graph. For example:"
msgstr ""
"Le tri obtenu peut dépendre de l'ordre dans lequel les éléments ont été "
"ajoutés dans le graphe. Par exemple :"

#: library/graphlib.rst:186
msgid ""
"This is due to the fact that \"0\" and \"2\" are in the same level in the "
"graph (they would have been returned in the same call to :meth:"
"`~TopologicalSorter.get_ready`) and the order between them is determined by "
"the order of insertion."
msgstr ""
"Ceci est dû au fait que \"0\" et \"2\" sont au même niveau dans le graphe "
"(ils auraient été renvoyés dans le même appel à :meth:`~TopologicalSorter."
"get_ready`) et l'ordre entre eux est déterminé par l'ordre lors de "
"l'insertion."

#: library/graphlib.rst:192
msgid "If any cycle is detected, :exc:`CycleError` will be raised."
msgstr "Si un circuit est détecté alors une :exc:`CycleError` est levée."

#: library/graphlib.rst:198
msgid "Exceptions"
msgstr "Exceptions"

#: library/graphlib.rst:199
msgid "The :mod:`graphlib` module defines the following exception classes:"
msgstr "Le module :mod:`graphlib` définit les classes d'exceptions suivantes :"

#: library/graphlib.rst:203
msgid ""
"Subclass of :exc:`ValueError` raised by :meth:`TopologicalSorter.prepare` if "
"cycles exist in the working graph. If multiple cycles exist, only one "
"undefined choice among them will be reported and included in the exception."
msgstr ""
"Une classe héritant de :exc:`ValueError` levée par :meth:`TopologicalSorter."
"prepare` si un circuit existe dans le graphe courant. Si plusieurs circuits "
"existent, un seul est inclus dans l'exception."

#: library/graphlib.rst:207
msgid ""
"The detected cycle can be accessed via the second element in the :attr:"
"`~CycleError.args` attribute of the exception instance and consists in a "
"list of nodes, such that each node is, in the graph, an immediate "
"predecessor of the next node in the list. In the reported list, the first "
"and the last node will be the same, to make it clear that it is cyclic."
msgstr ""
"On accède au circuit détecté via le second élément de l'attribut :attr:"
"`~CycleError.args` de l'instance de l'exception. Cet attribut est une liste "
"de nœuds où chaque nœud est, dans le graphe, un prédécesseur immédiat du "
"nœud suivant de la liste. Dans la liste renvoyée, le premier et le dernier "
"nœud sont les mêmes afin de bien indiquer que c'est un circuit."
